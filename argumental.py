#!/usr/bin/python3
# -*- coding: utf-8 -*-
from timeit import repeat


class NotArgumental:
    def __init__(self):
        self.twoFiftySix = 256

    def a(self):
        self.twoFiftySix / 2


class Argumental:
    def __init__(self):
        self.twoFiftySix = 256

    def a(self, twoFiftySix):
        twoFiftySix / 2


if __name__ == '__main__':
    print(min(repeat('NotArgumental().a()', number=1000000, globals=globals(), repeat=10)))
    # 0.42020149699999365

    print(min(repeat('Argumental().a(256)', number=1000000, globals=globals(), repeat=10)))
    # 0.3950516909999351

    #
    #
    #

    NA = NotArgumental()
    print(min(repeat('NA.a()', number=1000000, globals=globals(), repeat=10)))
    # 0.15799605899997005

    A = Argumental()
    print(min(repeat('A.a(256)', number=1000000, globals=globals(), repeat=10)))
    # 0.14152987300008135
