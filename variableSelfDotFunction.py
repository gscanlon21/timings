#!/usr/bin/python3
# -*- coding: utf-8 -*-
from timeit import repeat


class NotVariableSelfDotFunction:
    # def __init__(self):
        # pass

    def aToB(self):
        for __ in range(1000000):
            self.b()

    def b(self):
        pass


class VariableSelfDotFunction:
    # def __init__(self):
        # pass

    def aToB(self):
        selfB = self.b
        for __ in range(1000000):
            selfB()

    def b(self):
        pass


if __name__ == '__main__':
    print(min(repeat('NotVariableSelfDotFunction().aToB()', number=1, globals=globals(), repeat=10)))
    # 0.09486943000229076

    print(min(repeat('VariableSelfDotFunction().aToB()', number=1, globals=globals(), repeat=10)))
    # 0.06801294200704433
